import React from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import SharedLayout from './Components/SharedLayout'
import Home from './Components/Home'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Articles from './Components/Articles'
import SinglePage from './Components/SinglePage'
import Error from './Components/Error'
import data from './data'

class App extends React.Component {
  constructor(props) {
    super ()
    this.state = {

    }
  }
  render () {
    return (
      <BrowserRouter>
          <Routes>
            <Route path = '/' element={<SharedLayout></SharedLayout>}>
              <Route index element={<Home></Home>}></Route>
              <Route path='articles' element={<Articles></Articles>}></Route>
              <Route path='articles/:slugger' element={<SinglePage ></SinglePage>}></Route>
              <Route path='*' element={<Error></Error>}></Route>
            </Route> 
          </Routes>
      </BrowserRouter>
      
    )
  }
}

export default App
