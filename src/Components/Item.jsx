import React from "react";
import { Link } from "react-router-dom";

class Item extends React.Component {
    constructor(props) {
        super()
    }
    render () {
        return (
            <div className="item">
                <Link to={`${this.props.data.slug}`} ><p className="title item">{this.props.data.title}</p></Link>
                <p className="author item">{this.props.data.author}</p>
            </div>
        )
    }
}

export default Item